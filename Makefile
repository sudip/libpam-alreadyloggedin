#
#   Makefile for building Linux port of BSD's pam_alreadyloggedin module.
#
#   Written by Ilya Evseev using template from pam_mktemp package
#   by Solar Designer and Dmitry Levin.
#

CC = gcc
LD = ld
RM = rm -f
MKDIR = mkdir -p
INSTALL = install
#CFLAGS = -c -Wall -O2 -fPIC -I.
CFLAGS  = -c $(RPM_OPT_FLAGS) -fPIC -DLINUX_PAM -I. -Wall -DBUG_STAT_MISSING
LDFLAGS = -s -lpam --shared

TITLE = pam_alreadyloggedin
LIBSHARED = $(TITLE).so
SHLIBMODE = 700
SECUREDIR = /lib/security
FAKEROOT =
CONFMODE = 600
CONFDIR = /etc/pam.d
MANMODE = 444
ifndef MAN8DIR
MAN8DIR = /usr/share/man/man8
endif

OBJS = $(TITLE).o

all: $(LIBSHARED)

$(LIBSHARED): $(OBJS)
	$(LD) $(LDFLAGS) $(OBJS) -o $@

$(TITLE).o: $(TITLE).c
	$(CC) $(CFLAGS) $<

install:
	$(MKDIR) $(FAKEROOT)$(SECUREDIR)
	$(INSTALL) -m $(SHLIBMODE) $(LIBSHARED) $(FAKEROOT)$(SECUREDIR)
	$(MKDIR) $(FAKEROOT)$(CONFDIR)
	$(INSTALL) -m $(CONFMODE)   login.sso   $(FAKEROOT)$(CONFDIR)
	$(MKDIR) $(FAKEROOT)$(MAN8DIR)
	$(INSTALL) -m $(MANMODE)    $(TITLE).8  $(FAKEROOT)$(MAN8DIR)

uninstall: remove

remove:
	$(RM) $(FAKEROOT)$(SECUREDIR)/$(TITLE).so
	$(RM) $(FAKEROOT)$(CONFDIR)/login.sso
	$(RM) $(FAKEROOT)$(MAN8DIR)/$(TITLE).8

clean:
	$(RM) $(LIBSHARED) *.o *.s *.i

## EOF ##
